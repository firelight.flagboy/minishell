/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   minishell_struct.h                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/22 16:26:37 by fbenneto          #+#    #+#             */
/*   Updated: 2018/02/01 14:53:43 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MINISHELL_STRUCT_H
# define MINISHELL_STRUCT_H

# include "minishell_define.h"
# include "minishell_typedef.h"

struct	s_bult
{
	char			*name;
	int				(*s)(int argc, char **argv, t_env **environ);
};

struct	s_bool
{
	unsigned		x:1;
};

struct	s_env
{
	char			*key;
	char			*value;
	struct s_env	*next;
};

struct	s_argv
{
	char			s[2048];
	unsigned		use:1;
	struct s_argv	*next;
};
#endif
