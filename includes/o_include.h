/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   o_include.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/19 11:58:21 by fbenneto          #+#    #+#             */
/*   Updated: 2018/01/30 09:02:23 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef O_INCLUDE_H
# define O_INCLUDE_H

# include <sys/types.h>
# include <sys/stat.h>
# include <sys/wait.h>
# include <sys/time.h>
# include <sys/resource.h>
# include <limits.h>
# ifndef ARG_MAX
#  include <linux/limits.h>
# endif
# include <unistd.h>
# include <signal.h>
# include <dirent.h>
# include <stdlib.h>
# include <stdio.h>
# include <fcntl.h>

#endif
