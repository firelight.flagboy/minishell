/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   minishell_define.h                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/22 11:37:49 by fbenneto          #+#    #+#             */
/*   Updated: 2018/02/06 13:07:38 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MINISHELL_DEFINE_H
# define MINISHELL_DEFINE_H

# include "o_include.h"

# ifndef EXIT_STOP
#  define EXIT_STOP 2
# endif

# ifndef EXIT_WARNING
#  define EXIT_WARNING 3
# endif

# ifndef NB_BUILTIN
#  define NB_BUILTIN 9
# endif

# ifndef MAX_NAME_BUILT
#  define MAX_NAME_BUILT 128
# endif

# ifndef ARG_MAX
#  define ARG_MAX (256 * 1024)
# endif

# ifndef OPEN_MAX
#  define OPEN_MAX 10244
# endif

enum	e_type
{
	IDIR = (1 << 0),
	IREG = (1 << 1),
	ILNK = (1 << 2),
	IFIFO = (1 << 3),
	IBLK = (1 << 4),
	ISOCK = (1 << 5),
	ICHR = (1 << 6),
};
#endif
