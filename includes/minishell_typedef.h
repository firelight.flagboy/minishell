/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   minishell_typedef.h                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/22 16:26:12 by fbenneto          #+#    #+#             */
/*   Updated: 2018/02/16 09:03:09 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MINISHELL_TYPEDEF_H
# define MINISHELL_TYPEDEF_H

# include "minishell_struct.h"

/*
** Official
*/
typedef struct stat		t_stat;
typedef struct dirent	t_dirent;
/*
** Custom
*/
typedef struct s_bult	t_bult;
typedef struct s_bool	t_bool;
typedef struct s_env	t_env;
typedef struct s_argv	t_argv;
#endif
