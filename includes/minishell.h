/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   minishell.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/19 11:57:32 by fbenneto          #+#    #+#             */
/*   Updated: 2018/02/06 12:54:38 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MINISHELL_H
# define MINISHELL_H

# include "o_include.h"
# include "get_next_line.h"
# include "libft.h"
# include "ft_printf.h"
# include "my_errno.h"
# include "minishell_define.h"
# include "minishell_struct.h"
# include "minishell_typedef.h"

/*
** Global var for signal / pid
*/
pid_t	g_pid;
/*
** Minishell Fc
*/
int		ft_access(char *fullpath, int mode);
char	*routine_aps(t_argv *t, char *s, size_t i);
char	*routine_nor(t_argv *t, char *s, size_t i);
int		ft_get_argv(char *s, char *((*tab)[ARG_MAX + 1]));
char	**ft_get_option(int *ac, char **av, char *opt_s, size_t *opt);
int		ft_call_prog(int argc, char **argv, t_env **environ, int *res);
int		ft_direct_cmd(char **av, t_env **ev, int *res);
int		ft_type(char const *path, int type);
void	ft_promt(t_env **env, int *l_res);
void	ft_exit_code(int *res, char *name);
/*
** Lib C
*/
int		ft_isspace_2(int c);
char	*ft_itoa_cte(int n);
int		ft_atoi_cte(char const *s);
/*
** Signal
*/
void	handler(int sig);
/*
** Singleton
*/
int		*get_res();
/*
** Tchar
*/
void	ft_free_tchar(char **tab);
void	ft_free_tchar_d(char **tab[2]);
void	ft_free_tchar_node(char **tab);
size_t	ft_len_tchar(char **t);
/*
** Struct bult
*/
t_bult	*ft_init_tbult_s(void *data);
t_bult	*ft_get_tbult();
t_bult	*ft_init_tbult_name(void *data);
/*
** Struct env
*/
char	*ft_add_env(char *t, char *s, size_t *i);
t_env	**get_env();
void	ft_env_free_all(t_env **headref);
void	ft_env_free_one(t_env *node);
t_env	*ft_env_new(char *key, char *value);
void	ft_env_push(t_env **headref, char *key, char *value);
t_env	*ft_env_tab_list(char	**env);
size_t	ft_env_len(t_env *node);
char	*ft_env_node_str(t_env *node);
char	**ft_env_list_tab(t_env **headref);
void	ft_env_remove_if(t_env **headref, char *key);
void	ft_env_free_one(t_env *node);
/*
** Struct Argv
*/
t_argv	*ft_new_tlist(void);
size_t	ft_len_list(t_argv *head);
void	ft_free_targv(t_argv **headref);
char	*routine_get_argv(t_argv *t, char *s, size_t i);
/*
** Env
*/
char	*ft_getenv(t_env **env, char *key);
int		ft_setenv(t_env **env, char *key, char *val);
/*
** Builtin
*/
int		yes(int argc, char **argv, t_env **env);
int		pwd(int argc, char **argv, t_env **env);
int		echo(int argc, char **argv, t_env **env);
int		f_exit(int argc, char **argv, t_env **env);
int		clear(int argc, char **argv, t_env **env);
int		cd(int argc, char **argv, t_env **env);
int		env(int argc, char **argv, t_env **env);
int		ft_setenv_main(int argc, char **argv, t_env **env);
int		ft_unsetenv_main(int argc, char **argv, t_env **env);
#endif
