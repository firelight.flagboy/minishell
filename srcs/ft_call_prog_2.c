/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_call_prog_2.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/01 15:34:53 by fbenneto          #+#    #+#             */
/*   Updated: 2018/02/06 14:38:37 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

static int	ft_isexe(char *s)
{
	t_stat	stat;

	if (lstat(s, &stat))
		return (ft_warnx("minishell", "stat error"));
	if (S_ISREG(stat.st_mode))
		return (1);
	if (S_ISDIR(stat.st_mode))
		g_errn = ERR_ISDIR;
	else if (S_ISBLK(stat.st_mode))
		g_errn = ERR_ISBLK;
	else if (S_ISFIFO(stat.st_mode))
		g_errn = ERR_ISFIFO;
	else if (S_ISLNK(stat.st_mode))
		g_errn = ERR_ISLNK;
	else if (S_ISSOCK(stat.st_mode))
		g_errn = ERR_ISSOCK;
	else if (S_ISCHR(stat.st_mode))
		g_errn = ERR_ISCHR;
	return (0);
}

int			ft_fork_direct(char **av, char **ev, int *res)
{
	char	buf[2048];

	ft_strcpy(buf, av[0]);
	g_pid = fork();
	if (g_pid == 0)
	{
		if (execve(buf, av, ev) == -1)
			ft_error("execve", "error");
	}
	else
	{
		if (waitpid(g_pid, res, 0) == -1)
			return (ft_warnx("waitpid", "error"));
		ft_exit_code(res, av[0]);
	}
	return (*res);
}

int			ft_direct_cmd(char **av, t_env **ev, int *res)
{
	int		st;
	char	**env;

	*res = 0;
	if ((st = ft_access(av[0], X_OK)) == 0 && ft_isexe(av[0]) > 0)
	{
		env = ft_env_list_tab(ev);
		ft_fork_direct(av, env, res);
		ft_free_tchar(env);
		return (*res);
	}
	return (ft_warn2_errn("minishell", av[0], g_errn));
}
