/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_promt.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/06 12:31:10 by fbenneto          #+#    #+#             */
/*   Updated: 2018/02/06 13:25:48 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

static int	ft_argv_tores(char *line, int *res)
{
	*res = 0;
	while (*line)
	{
		if (ft_isspace_2(*line) == 0)
			return (1);
		line++;
	}
	return (0);
}

static void	ft_boucle(char **mulcmd, int *l_res, t_env **env)
{
	char	*cmd[ARG_MAX + 1];
	int		res;
	int		i;

	i = 0;
	while (mulcmd[i])
	{
		if (ft_argv_tores(mulcmd[i], &res)\
		&& (res = ft_get_argv(mulcmd[i], &cmd)) == -1)
			ft_warnx_errn("minishell", g_errn);
		if (*mulcmd[i] == 0)
			*l_res = 0;
		if (*mulcmd[i] && res > 0)
			*l_res = ft_call_prog(res, cmd, env, l_res);
		if (res > 0)
			ft_free_tchar_node(cmd);
		free(mulcmd[i]);
		i++;
	}
}

void		ft_promt(t_env **env, int *l_res)
{
	char	**mulcmd;
	char	*line;
	char	buf[2048];

	ft_dprintf(2, "\033[2m-----------------------------"
	"------------------------\033[0m\n"
	"%s%s\033[0m\n$>",\
	(*l_res) ? "\033[31m" : "\033[32m", getcwd(buf, 2048));
	if (get_next_line(0, &line) < 0)
		ft_error("minishell", "get_next_line error");
	if (!(mulcmd = ft_strsplit(line, 59)))
		ft_warnx_errn("minishell", g_errn);
	ft_boucle(mulcmd, l_res, env);
	free(mulcmd);
	free(line);
}
