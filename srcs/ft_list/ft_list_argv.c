/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_list_argv.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/30 12:53:27 by fbenneto          #+#    #+#             */
/*   Updated: 2018/01/31 09:54:09 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

t_argv	*ft_new_tlist(void)
{
	t_argv *node;

	if (!(node = (t_argv*)malloc(sizeof(t_argv))))
		return (NULL);
	node->next = NULL;
	node->s[0] = 0;
	node->use = 0;
	return (node);
}

size_t	ft_len_list(t_argv *head)
{
	size_t	i;

	i = 0;
	while (head)
	{
		head = head->next;
		i++;
	}
	return (i);
}

void	ft_free_targv(t_argv **headref)
{
	t_argv	*node;
	t_argv	*tmp;

	node = *headref;
	while (node)
	{
		tmp = node;
		node = node->next;
		free(tmp);
	}
	*headref = NULL;
}
