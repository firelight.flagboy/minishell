/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_list_env_2.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/24 12:34:38 by fbenneto          #+#    #+#             */
/*   Updated: 2018/02/06 14:38:30 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

char		*ft_env_node_str(t_env *node)
{
	size_t	lk;
	size_t	lv;
	char	*res;

	if (node == NULL || node->key == NULL || node->value == NULL)
	{
		g_errn = ERR_FAULT;
		return (NULL);
	}
	lk = ft_strlen(node->key);
	lv = ft_strlen(node->value);
	if (!(res = (char*)malloc((lk + lv + 2) * sizeof(char))))
	{
		g_errn = ERR_MALLOC;
		return (NULL);
	}
	ft_strcpy(res, node->key);
	ft_strcat(res, "=");
	ft_strcat(res, node->value);
	return (res);
}

char		**ft_env_list_tab(t_env **headref)
{
	char	**t;
	t_env	*node;
	size_t	l;

	if (headref == NULL || *headref == NULL)
		return (NULL);
	node = *headref;
	l = ft_env_len(node);
	if (!(t = (char**)malloc((l + 1) * sizeof(char*))))
	{
		g_errn = ERR_MALLOC;
		return (NULL);
	}
	t[l] = NULL;
	l = 0;
	while (node)
	{
		if (!(t[l++] = ft_env_node_str(node)))
			ft_error_errn("environ", g_errn);
		node = node->next;
	}
	return (t);
}

void		ft_env_free_one(t_env *node)
{
	free(node->key);
	free(node->value);
	free(node);
}

void		ft_env_remove_if(t_env **headref, char *key)
{
	t_env *node;
	t_env *prev;

	node = *headref;
	prev = NULL;
	while (node)
	{
		if (ft_strcmp(key, node->key) == 0)
		{
			if (prev == NULL)
			{
				*headref = node->next;
				ft_env_free_one(node);
				node = *headref;
				break ;
			}
			prev->next = node->next;
			ft_env_free_one(node);
			node = prev;
			break ;
		}
		prev = node;
		node = node->next;
	}
}
