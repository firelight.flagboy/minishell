/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_list_env.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/24 09:24:40 by fbenneto          #+#    #+#             */
/*   Updated: 2018/02/01 15:41:42 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

void		ft_env_free_all(t_env **headref)
{
	t_env	*node;
	t_env	*tmp;

	if (headref == NULL || *headref == NULL)
		return ;
	node = *headref;
	while (node)
	{
		tmp = node;
		node = node->next;
		free(tmp->key);
		free(tmp->value);
		free(tmp);
	}
	*headref = NULL;
}

t_env		*ft_env_new(char *key, char *value)
{
	t_env *node;

	if ((key == NULL || value == NULL))
	{
		g_errn = ERR_FAULT;
		return (NULL);
	}
	if (!(node = (t_env*)malloc(sizeof(t_env))))
	{
		g_errn = ERR_MALLOC;
		return (NULL);
	}
	if (!(node->key = ft_strdup(key)))
	{
		g_errn = ERR_MALLOC;
		return (NULL);
	}
	if (!(node->value = ft_strdup(value)))
	{
		g_errn = ERR_MALLOC;
		return (NULL);
	}
	node->next = 0;
	return (node);
}

void		ft_env_push(t_env **headref, char *key, char *value)
{
	t_env *node;

	if (!(node = ft_env_new(key, value)))
		ft_error_errn("environ", g_errn);
	node->next = *headref;
	*headref = node;
}

t_env		*ft_env_tab_list(char **env)
{
	t_env	*head;
	char	*tmp;

	head = NULL;
	if (*env)
	{
		tmp = ft_strchr(*env, '=');
		*tmp = 0;
		ft_env_push(&head, *env, tmp + 1);
		env++;
	}
	while (*env)
	{
		tmp = ft_strchr(*env, '=');
		*tmp = 0;
		ft_env_push(&head, *env, tmp + 1);
		env++;
	}
	return (head);
}

size_t		ft_env_len(t_env *node)
{
	size_t	l;

	l = 0;
	while (node)
	{
		node = node->next;
		l++;
	}
	return (l);
}
