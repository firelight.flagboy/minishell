/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_type.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/06 10:56:06 by fbenneto          #+#    #+#             */
/*   Updated: 2018/02/06 11:12:26 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

int		ft_type(char const *path, int type)
{
	t_stat	stat;

	if (lstat(path, &stat))
		return (ft_warnx("minishell", "stat error"));
	if (S_ISREG(stat.st_mode) && (type & IREG) == 0 && (g_errn = ERR_ISREG))
		return (EXIT_FAILURE);
	if (S_ISDIR(stat.st_mode) && (type & IDIR) == 0 && (g_errn = ERR_ISDIR))
		return (EXIT_FAILURE);
	if (S_ISBLK(stat.st_mode) && (type & IBLK) == 0 && (g_errn = ERR_ISBLK))
		return (EXIT_FAILURE);
	if (S_ISFIFO(stat.st_mode) && (type & IFIFO) == 0 && (g_errn = ERR_ISFIFO))
		return (EXIT_FAILURE);
	if (S_ISLNK(stat.st_mode) && (type & ILNK) == 0 && (g_errn = ERR_ISLNK))
		return (EXIT_FAILURE);
	if (S_ISSOCK(stat.st_mode) && (type & ISOCK) == 0 && (g_errn = ERR_ISSOCK))
		return (EXIT_FAILURE);
	if (S_ISCHR(stat.st_mode) && (type & ICHR) == 0 && (g_errn = ERR_ISCHR))
		return (EXIT_FAILURE);
	return (0);
}
