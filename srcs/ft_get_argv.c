/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_get_argv.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/30 12:49:37 by fbenneto          #+#    #+#             */
/*   Updated: 2018/02/01 15:39:47 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

int			ft_isspace_2(int c)
{
	return (c == ' ' || c == '\t' || c == '\n' || c == '\r' || c == '\v');
}

char		*routine_get_argv(t_argv *t, char *s, size_t i)
{
	while (*s && ft_isspace_2(*s))
		s++;
	if (*s == 0)
		t->use = 0;
	else
		t->use = 1;
	if (*s == 0x22)
		return (routine_aps(t, s + 1, i));
	else
		return (routine_nor(t, s, i));
}

int			ft_return(char *((*t)[ARG_MAX + 1]), t_argv **headref, size_t i)
{
	char	**b;
	t_argv	*node;
	size_t	len;

	b = *t;
	i = 0;
	node = *headref;
	if (i > ARG_MAX && (g_errn = ERR_ARGTOOMUCH))
		return (-1);
	while (node)
	{
		len = ft_strlen(node->s);
		if (node->use)
		{
			if (!(b[i] = (char*)malloc((len + 1) * sizeof(char)))\
			&& (g_errn = ERR_MALLOC))
				return (-1);
			ft_strncpy(b[i++], node->s, len);
		}
		node = node->next;
	}
	ft_free_targv(headref);
	b[i] = 0;
	return (ft_len_tchar(b));
}

static int	ft_sort(t_argv **head)
{
	t_argv	*node;
	t_argv	*tmp;

	node = *head;
	while (node)
	{
		tmp = node;
		node = node->next;
		free(tmp);
	}
	return (-1);
}

int			ft_get_argv(char *s, char *((*t)[ARG_MAX + 1]))
{
	long	i;
	t_argv	*head;
	t_argv	*node;

	if (!(head = ft_new_tlist()))
		return (-1);
	node = head;
	if ((s = routine_get_argv(node, s, 0)) == NULL)
		return (ft_sort(&head));
	i = 0;
	while (*s != '\0')
	{
		if (i > ARG_MAX)
			return (ft_sort(&head));
		if (!(node->next = ft_new_tlist()))
			return (ft_sort(&head));
		node = node->next;
		if ((s = routine_get_argv(node, s, 0)) == NULL)
			return (ft_sort(&head));
		i++;
	}
	i = ft_len_list(head);
	if ((i = ft_return(t, &head, i)) == -1)
		return (-1);
	return (i);
}
