/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_get_argv_2.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/30 12:50:48 by fbenneto          #+#    #+#             */
/*   Updated: 2018/02/06 14:14:37 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

char		*ft_add_env(char *t, char *s, size_t *i)
{
	char	*env;
	char	env_n[1024];
	int		y;

	s++;
	y = 0;
	while (*s && (*s >= 0x21 && *s <= 0x7f) && *s != 0x2f)
		env_n[y++] = *s++;
	env_n[y] = 0;
	t[*i] = 0;
	if (ft_strcmp(env_n, "?") == 0)
		env = ft_itoa_cte(*get_res());
	else if (!(env = ft_getenv(get_env(), env_n)))
		return (s);
	y = 0;
	while (*env)
		t[*i + y++] = *env++;
	t[*i + y] = 0;
	*i = *i + y;
	return (s - 1);
}

char		*routine_aps(t_argv *t, char *s, size_t i)
{
	char	*d;

	d = s;
	while (*d && *d != 0x22)
		d++;
	if (*d == 0 && (g_errn = ERR_PARSING))
		return (NULL);
	if (*s == '~' && (s[1] == 0 || s[1] == '/'))
	{
		ft_add_env(t->s, "$HOME", &i);
		s++;
	}
	while (*s && *s != 0x22)
	{
		if (*s == '$')
			s = ft_add_env(t->s, s, &i);
		else
			t->s[i++] = *s;
		s++;
	}
	t->s[i] = '\0';
	if (*s == 0x22 && s[1] != 0 && ft_isspace_2(s[1]) == 0)
		return (routine_get_argv(t, s + 1, i));
	return ((*s) ? s + 1 : s);
}

char		*routine_nor(t_argv *t, char *s, size_t i)
{
	if (*s == '~' && (s[1] == 0 || s[1] == '/'))
	{
		ft_add_env(t->s, "$HOME", &i);
		s++;
	}
	while (*s && ft_isspace_2(*s) == 0)
	{
		if (*s == 0x22)
			return (routine_get_argv(t, s, i));
		else if (*s == '$')
			s = ft_add_env(t->s, s, &i);
		else if (*s == 0x5c && *(s + 1) != '\0')
			t->s[i++] = *(1 + s++);
		else if (*s != 0x5c)
			t->s[i++] = *s;
		s++;
	}
	t->s[i] = '\0';
	return (s);
}
