/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_init_tbult.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/22 16:30:06 by fbenneto          #+#    #+#             */
/*   Updated: 2018/01/31 15:49:03 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

t_bult	*ft_init_tbult_s(void *data)
{
	t_bult *b;

	b = (t_bult*)data;
	b[0].s = f_exit;
	b[1].s = echo;
	b[2].s = cd;
	b[3].s = ft_setenv_main;
	b[4].s = ft_unsetenv_main;
	b[5].s = env;
	b[6].s = clear;
	b[7].s = pwd;
	b[8].s = yes;
	return (b);
}

t_bult	*ft_init_tbult_name(void *data)
{
	t_bult *b;

	b = (t_bult*)data;
	b[0].name = "exit";
	b[1].name = "echo";
	b[2].name = "cd";
	b[3].name = "setenv";
	b[4].name = "unsetenv";
	b[5].name = "env";
	b[6].name = "clear";
	b[7].name = "pwd";
	b[8].name = "yes";
	b[9].name = NULL;
	return (b);
}
