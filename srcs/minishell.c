/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   minishell.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/19 11:57:10 by fbenneto          #+#    #+#             */
/*   Updated: 2018/02/09 08:53:13 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

void		ft_call_prompt(void)
{
	int		*res;
	t_env	**env;

	res = get_res();
	env = get_env();
	while (1)
	{
		ft_promt(env, res);
		g_pid = 0;
	}
	ft_env_free_all(env);
	exit(*get_res());
}

void		handler(int sig)
{
	char	buf[PATH_MAX];
	int		*st;

	signal(SIGINT, handler);
	st = get_res();
	*st = EXIT_WARNING;
	if (g_pid != 0)
	{
		kill(g_pid, sig);
		ft_printf("\n");
		return ;
	}
	else
		ft_dprintf(2, "\n\033[2m-----------------------------"
		"------------------------\033[0m\n"
		"%s%s\033[0m\n$>",\
		(*st) ? "\033[31m" : "\033[32m", getcwd(buf, PATH_MAX));
}

int			main(int argc, char **argv, char **environ)
{
	t_env	**env;
	char	*shlv;
	char	buf[PATH_MAX];
	int		shlvl;

	(void)argc;
	env = get_env();
	signal(SIGINT, handler);
	clear(0, NULL, NULL);
	*env = ft_env_tab_list(environ);
	if (!(shlv = ft_getenv(env, "PWD")))
		ft_setenv(env, "PWD", getcwd(buf, PATH_MAX));
	if (!(shlv = ft_getenv(env, "_")))
		ft_setenv(env, "_", ft_strcat(ft_strcat(buf, "/"), argv[0]));
	if (!(shlv = ft_getenv(env, "SHLVL")))
		ft_setenv(env, "SHLVL", "1");
	else
	{
		shlvl = ft_atoi_cte(shlv);
		ft_setenv(env, "SHLVL", ft_itoa_cte(++shlvl));
	}
	ft_call_prompt();
	ft_env_free_all(env);
	return (0);
}
