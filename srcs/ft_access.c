/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_access.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/25 09:08:00 by fbenneto          #+#    #+#             */
/*   Updated: 2018/02/01 15:31:11 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

static void	ft_tofind(char *tofind, char *s)
{
	while (*s != '/' && *s)
		*tofind++ = *s++;
	*tofind = 0;
}

static int	ft_find(char *buf, char *s)
{
	char		tofind[2048];
	char		*tmp;
	DIR			*dir;
	t_dirent	*dirent;

	if (access(buf, X_OK) == -1 && (g_errn = ERR_ACCESS))
		return (-1);
	if (!(tmp = ft_strchr(s, '/')))
		ft_tofind(tofind, s);
	else
		ft_tofind(tofind, tmp + 1);
	if (!(dir = opendir(buf))\
	&& (g_errn = ERR_NOENT))
		return (-1);
	while ((dirent = readdir(dir)))
		if (ft_strcmp(dirent->d_name, tofind) == 0)
		{
			closedir(dir);
			return (1);
		}
	g_errn = ERR_NOENT;
	closedir(dir);
	return (0);
}

static int	ft_here(char *fullpath, int mode)
{
	char	buff[2048];

	ft_strcpy(buff, "./");
	if (ft_strcmp(fullpath, "../") == 0)
		ft_strcat(buff, "..");
	else
		ft_strcat(buff, fullpath);
	return (ft_access(buff, mode));
}

int			ft_access(char *full, int mode)
{
	char	buf[2048];
	int		res;
	char	*tmp[2];

	if (ft_strcmp(full, "/") == 0)
		return ((access(full, mode) == -1 && (g_errn = ERR_ACCESS)) ? -1 : 0);
	if (full[0] != '/' && ft_strncmp(full, "./", 2))
		return (ft_here(full, mode));
	tmp[1] = ft_strchr(full, '/');
	buf[0] = 0;
	tmp[0] = full;
	while (ft_strcmp(full, buf) != 0)
	{
		if (tmp[1])
			ft_strncat(buf, tmp[0], tmp[1] - tmp[0] + 1);
		else if (ft_strncat(buf, tmp[0], 2048))
			break ;
		if (ft_find(buf, tmp[0]) < 1)
			return (-1);
		tmp[0] = tmp[1] + 1;
		tmp[1] = ft_strchr(tmp[0], '/');
	}
	if ((res = access(full, mode)) == -1)
		g_errn = ERR_ACCESS;
	return (res);
}
