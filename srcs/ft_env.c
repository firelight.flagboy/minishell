/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_env.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/22 10:35:30 by fbenneto          #+#    #+#             */
/*   Updated: 2018/02/01 15:36:59 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

char	*ft_getenv(t_env **env, char *key)
{
	t_env	*node;

	node = *env;
	while (node)
	{
		if (ft_strcmp(node->key, key) == 0)
			return (node->value);
		node = node->next;
	}
	return (NULL);
}

int		ft_setenv(t_env **env, char *key, char *val)
{
	t_env	*node;
	t_env	*prev;

	node = *env;
	prev = NULL;
	while (node)
	{
		if (ft_strcmp(node->key, key) == 0)
		{
			free(node->value);
			if (!(node->value = ft_strdup(val)))
				ft_error_errn("setenv", g_errn);
			return (0);
		}
		prev = node;
		node = node->next;
	}
	if (prev == NULL)
		*env = ft_env_new(key, val);
	else if (!(prev->next = ft_env_new(key, val)))
		ft_error_errn("setenv", g_errn);
	return (0);
}
