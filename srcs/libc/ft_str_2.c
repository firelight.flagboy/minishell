/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_str_2.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/19 13:30:48 by fbenneto          #+#    #+#             */
/*   Updated: 2018/02/01 15:43:27 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strsub(char const *s, size_t start, size_t len)
{
	unsigned char	*res;
	unsigned char	*str;
	size_t			*r;
	size_t			*f;
	size_t			i;

	if (s == NULL)
		return (NULL);
	if (!(res = (unsigned char*)malloc((len + 1) * sizeof(unsigned char))))
		return (NULL);
	f = (size_t*)s;
	r = (size_t*)res;
	i = 0;
	while (i < len / sizeof(size_t))
		r[i++] = *((start / sizeof(size_t)) + f++);
	str = (unsigned char*)res + len - len % sizeof(size_t);
	s = s + len - len % sizeof(size_t);
	i = 0;
	while (i < len % sizeof(size_t))
		str[i++] = *((start % sizeof(size_t)) + s++);
	str[i] = 0;
	return ((char*)res);
}

char	*ft_strdup(char const *s)
{
	unsigned char	*res;
	size_t			len;
	size_t			i;

	if (s == NULL)
		return (NULL);
	len = ft_strlen(s);
	if (!(res = (unsigned char*)malloc((len + 1) * sizeof(unsigned char))))
		return (NULL);
	i = 0;
	while (*s)
		res[i++] = *s++;
	res[i] = 0;
	return ((char*)res);
}

char	*ft_strcpy(char *d, char const *s)
{
	char *str;

	str = d;
	while (*s)
		*d++ = *s++;
	*d = 0;
	return (str);
}

char	*ft_strncpy(char *d, char const *s, size_t len)
{
	char *str;

	str = d;
	while (*s && len)
	{
		*d++ = *s++;
		len--;
	}
	*d = 0;
	return (str);
}

char	*ft_strcat(char *d, char const *s)
{
	char *str;

	str = d;
	while (*d)
		d++;
	while (*s)
		*d++ = *s++;
	*d = 0;
	return (str);
}
