/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_tchar.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/22 12:46:47 by fbenneto          #+#    #+#             */
/*   Updated: 2018/01/31 09:10:50 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

void	ft_free_tchar(char **tab)
{
	size_t	i;

	if (!tab || *tab == NULL)
		return ;
	i = 0;
	while (tab[i])
		free(tab[i++]);
	free(tab);
}

void	ft_free_tchar_node(char **tab)
{
	size_t	i;

	if (!tab || *tab == NULL)
		return ;
	i = 0;
	while (tab[i])
		free(tab[i++]);
}

void	ft_free_tchar_d(char **t[2])
{
	ft_free_tchar(t[0]);
	ft_free_tchar(t[1]);
}

size_t	ft_len_tchar(char **t)
{
	size_t	i;

	i = 0;
	while (t[i])
		i++;
	return (i);
}
