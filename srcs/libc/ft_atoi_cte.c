/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi_cte.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/31 15:17:30 by fbenneto          #+#    #+#             */
/*   Updated: 2018/02/01 15:31:37 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

int		ft_atoi_cte(char const *s)
{
	int		i;

	i = 0;
	while (*s >= '0' && *s <= '9')
	{
		i = (i * 10) + (*s - '0');
		s++;
	}
	return (i);
}

char	*ft_itoa_cte(int n)
{
	static char	s[33];
	size_t		len;

	len = ft_len_nb((intmax_t)n, 10);
	s[len] = 0;
	len--;
	while ((long)len >= 0)
	{
		s[len] = (n % 10) + '0';
		n /= 10;
		len--;
	}
	return (s);
}
