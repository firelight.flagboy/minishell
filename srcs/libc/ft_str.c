/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_str.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/19 12:15:23 by fbenneto          #+#    #+#             */
/*   Updated: 2018/01/25 10:11:33 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_bzero(void *s, size_t len)
{
	unsigned char	*t;
	size_t			*f;
	size_t			i;

	if (s == NULL)
		return ;
	f = s;
	i = 0;
	while (i < len / sizeof(size_t))
		f[i++] = 0;
	t = (unsigned char *)s + len - len % sizeof(size_t);
	i = 0;
	while (i < len % sizeof(size_t))
		t[i++] = 0;
}

char	*ft_strjoin(char const *s1, char const *s2)
{
	size_t	l1;
	size_t	l2;
	size_t	i;
	char	*res;

	if (s1 == NULL || s2 == NULL)
		return (NULL);
	l1 = ft_strlen(s1);
	l2 = ft_strlen(s2);
	if (!(res = (char*)malloc((l1 + l2 + 1) * sizeof(char))))
		return (NULL);
	i = 0;
	while (*s1)
		res[i++] = *s1++;
	while (*s2)
		res[i++] = *s2++;
	res[i] = 0;
	return (res);
}

void	ft_strdel(char **ts)
{
	if (ts == NULL || *ts == NULL)
		return ;
	free(*ts);
	*ts = NULL;
}

char	*ft_strnew(size_t len)
{
	char	*res;

	if (!(res = (char*)malloc((len + 1) * sizeof(char))))
		return (NULL);
	ft_bzero(res, (len + 1) * sizeof(char));
	return (res);
}

char	*ft_strncat(char *d, char const *s, size_t n)
{
	char	*r;

	r = d;
	while (*d)
		d++;
	while (*s && n > 0)
	{
		*d++ = *s++;
		n--;
	}
	*d = 0;
	return (d);
}
