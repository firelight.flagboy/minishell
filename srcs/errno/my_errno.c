/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   my_errno.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/22 13:40:43 by fbenneto          #+#    #+#             */
/*   Updated: 2018/02/06 14:04:15 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "my_errno.h"

static char	*ft_unknown_code(int errnum)
{
	static char	s[64];
	static char	*p;
	int			i;
	int			l;

	if (!p)
		p = " : code error unknown";
	l = ft_atoi_to_buff(errnum, s);
	i = 0;
	while (p[i])
	{
		s[l + i] = p[i];
		i++;
	}
	s[l + i] = 0;
	return (s);
}

int			ft_iscritical(int errnum)
{
	if (errnum == ERR_MALLOC || errnum == ERR_FORK)
		return (1);
	return (0);
}

char		*ft_strerror2(int errnum)
{
	if (errnum == ERR_BADARG)
		return ("bad argument given");
	if (errnum == ERR_AGRMISSING)
		return ("missing arguments");
	if (errnum == ERR_ARGTOOMUCH)
		return ("too much arguments");
	if (errnum == ERR_NOCMD)
		return ("command not found");
	if (errnum == ERR_ISBLK)
		return ("is a block");
	if (errnum == ERR_ISCHR)
		return ("is a char file");
	if (errnum == ERR_ISFIFO)
		return ("is a pipe");
	if (errnum == ERR_ISLNK)
		return ("is a link");
	if (errnum == ERR_ISSOCK)
		return ("is a socket file");
	if (errnum == ERR_NOPATH)
		return ("no path set");
	if (errnum == ERR_ISREG)
		return ("is a file");
	if (errnum == ERR_CHDIR)
		return ("chdir error");
	return (ft_unknown_code(errnum));
}

char		*ft_strerror(int errnum)
{
	g_errn = ERR_OK;
	if (errnum == ERR_OK)
		return ("so far so good");
	if (errnum == ERR_MALLOC)
		return ("malloc error");
	if (errnum == ERR_NOENT)
		return ("no such file or directory");
	if (errnum == ERR_ACCESS)
		return ("Permission denied");
	if (errnum == ERR_NAMETOOLONG)
		return ("File name too long");
	if (errnum == ERR_SIZE)
		return ("size too big");
	if (errnum == ERR_ISNOTDIR)
		return ("is not a directory");
	if (errnum == ERR_ISDIR)
		return ("is a directory");
	if (errnum == ERR_FAULT)
		return ("bad adress");
	if (errnum == ERR_FORK)
		return ("error on fork");
	if (errnum == ERR_PARSING)
		return ("bad parsing");
	return (ft_strerror2(errnum));
}

char		*ft_strsig(int sig)
{
	if (sig == SIGABRT || sig == SIGIO)
		return ("abort");
	if (sig == SIGBUS)
		return ("bus error");
	if (sig == SIGSEGV)
		return ("segmentation fault");
	ft_printf("error code %d\n", sig);
	return ("unknown code");
}
