/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_singleton.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/22 15:47:19 by fbenneto          #+#    #+#             */
/*   Updated: 2018/02/01 15:42:50 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

t_bult	*ft_get_tbult(void)
{
	static t_bult	b[NB_BUILTIN + 1];

	if (b[0].name == NULL)
	{
		ft_init_tbult_name(b);
		ft_init_tbult_s(b);
	}
	return (b);
}

t_env	**get_env(void)
{
	static t_env *env;

	return (&env);
}

int		*get_res(void)
{
	static int res;

	return (&res);
}
