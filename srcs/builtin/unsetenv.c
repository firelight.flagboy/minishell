/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   unsetenv.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/24 14:43:52 by fbenneto          #+#    #+#             */
/*   Updated: 2018/02/06 16:22:37 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

int		ft_unsetenv_main(int argc, char **argv, t_env **env)
{
	t_env	*node;
	int		i;

	i = 0;
	argc--;
	while (i < argc)
		ft_env_remove_if(env, argv[1 + i++]);
	node = *env;
	while (node)
	{
		ft_printf("%s=%s\n", node->key, node->value);
		node = node->next;
	}
	return (0);
}
