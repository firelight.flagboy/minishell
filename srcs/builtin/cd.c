/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cd.c                                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/23 15:13:10 by fbenneto          #+#    #+#             */
/*   Updated: 2018/02/09 08:52:55 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

char		**get_opt_cd(char *name, char **av, int *ac)
{
	int	i;

	i = -1;
	while (av[++i])
	{
		if (av[i][0] != '-')
			break ;
		if (ft_strcmp(av[i], "--") == 0 && (i = i + 1))
			break ;
		else if (ft_strncmp(av[i], "--", 2) == 0)
			break ;
		if (av[i][0] == '-' && (av[i][1] != 0))
		{
			ft_bad_option(name, av[i][1]);
			return (NULL);
		}
		else
			break ;
	}
	*ac = *ac - i;
	return (av + i);
}

static char	*ft_remove_trailing_slahs(char *s)
{
	char	*d;

	if (*s == '/' && s[1] == 0)
		return (s);
	if (!(d = ft_strrchr(s, '/')))
		return (s);
	if (d[1] == 0)
		d[0] = 0;
	return (s);
}

static int	routine(char *buf, char **pwd, int argc, char *name)
{
	if (argc > 2)
		return (ft_warnx_errn(name, (g_errn = ERR_ARGTOOMUCH)));
	if (argc == 1)
		ft_strcpy_warnx(buf, pwd[0], 2048);
	else if (ft_strcmp(pwd[3], "-") == 0)
		ft_strcpy_warnx(buf, pwd[2], 2048);
	else
		ft_strcpy_warnx(buf, ft_remove_trailing_slahs(pwd[3]), 2048);
	return (0);
}

int			cd(int argc, char **argv, t_env **env)
{
	char	*pwd[5];
	char	buf[PATH_MAX];
	char	cwd[PATH_MAX];
	int		res;

	pwd[4] = argv[0];
	if (!(argv = get_opt_cd(pwd[4], ++argv, &argc)))
		return (ft_usage(pwd[4], "[directory]"));
	pwd[3] = argv[0];
	if (!(pwd[0] = ft_getenv(env, "HOME")) && argv[0] == NULL)
		return (ft_warnx(pwd[4], "HOME not set"));
	pwd[1] = ft_getenv(env, "PWD");
	if (!(pwd[2] = ft_getenv(env, "OLDPWD")) && ft_strcmp(argv[0], "-") == 0)
		return (ft_warnx(pwd[4], "OLDPWD not set"));
	if ((res = routine(buf, pwd, argc, pwd[4])) != 0)
		return (res);
	if (ft_access(buf, X_OK) || ft_type(buf, IDIR))
		return (ft_warn2_errn(pwd[4], pwd[3], g_errn));
	if ((getcwd(cwd, PATH_MAX)) == NULL || ft_setenv(env, "OLDPWD", cwd) == -1)
		return (ft_warnx_errn(pwd[4], g_errn));
	if (chdir(buf) == -1)
		return (ft_warnx_errn(pwd[4], (g_errn = ERR_CHDIR)));
	if (getcwd(cwd, PATH_MAX) == NULL || ft_setenv(env, "PWD", cwd) == -1)
		return (ft_warnx_errn(pwd[4], g_errn));
	return (EXIT_SUCCESS);
}
