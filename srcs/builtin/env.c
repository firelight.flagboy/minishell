/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   env.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/24 13:28:21 by fbenneto          #+#    #+#             */
/*   Updated: 2018/02/06 14:11:57 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

int			rd_ft(char *name, char *s, int *opt)
{
	char	*t;
	int		i;

	t = "i";
	while (*s)
	{
		i = -1;
		while (t[++i])
			if (t[i] == *s)
			{
				*opt = *opt | (1 << i);
				break ;
			}
		if (t[i] == 0)
			return (ft_bad_option(name, *s));
		s++;
	}
	return (0);
}

char		**get_opt_env(char *name, char **av, int *ac, int *opt)
{
	int	i;

	i = 0;
	*opt = 0;
	while (av[i])
	{
		if (av[i][0] != '-')
			break ;
		if (ft_strcmp(av[i], "--") == 0 && (i = i + 1))
			break ;
		if (av[i][0] == '-')
		{
			if (rd_ft(name, av[i] + 1, opt))
				return (NULL);
		}
		else
			break ;
		i++;
	}
	*ac = *ac - i;
	return (av + i);
}

int			ft_binary(char **av, t_env **env)
{
	int		res;
	int		i;

	i = ft_len_tchar(av);
	if ((res = ft_call_prog(i, av, env, &res)) == EXIT_WARNING)
		return (EXIT_WARNING);
	return (res);
}

int			env(int argc, char **argv, t_env **env)
{
	t_env	*node;
	char	*name;
	int		opt;

	name = argv[0];
	if (!(argv = get_opt_env(name, ++argv, &argc, &opt)))
		return (ft_usage(name, "[-i] [command [argument]]"));
	node = *env;
	if (opt & 0b1)
		node = NULL;
	if (*argv)
		return (ft_binary(argv, &node));
	while (node)
	{
		ft_printf("%s=%s\n", node->key, node->value);
		node = node->next;
	}
	return (EXIT_SUCCESS);
}
