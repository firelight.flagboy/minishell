/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   echo.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/23 11:01:36 by fbenneto          #+#    #+#             */
/*   Updated: 2018/02/01 14:37:31 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

static int	ft_init_opt(char *opt, int *res)
{
	char	*charset;
	int		i[2];

	charset = "n";
	while (*opt)
	{
		i[0] = 0;
		i[1] = 0;
		while (charset[i[0]])
		{
			if (*opt == charset[i[0]] && (i[1] = 1))
				*res = *res | (1 << i[0]);
			i[0]++;
		}
		if (i[1] == 0)
		{
			*res = 0;
			return (-1);
		}
		opt++;
	}
	return (0);
}

static void	ft_get_opt_echo(int *argc, char ***argv, int *opt)
{
	int		i;
	int		res;
	char	**ts;

	i = 0;
	res = 0;
	ts = *argv;
	while (ts[i])
	{
		if (ts[i][0] != '-')
			break ;
		if (ft_init_opt((ts[i] + 1), &res) == -1)
			break ;
		i++;
	}
	*argv = *argv + i;
	*argc = *argc - i;
	*opt = res;
}

int			echo(int argc, char **argv, t_env **env)
{
	int		opt;

	(void)env;
	if (argc == 1)
	{
		ft_printf("\n");
		return (EXIT_SUCCESS);
	}
	opt = 0;
	argc--;
	argv++;
	ft_get_opt_echo(&argc, &argv, &opt);
	while (argc > 0)
	{
		argc--;
		ft_printf("%s", *argv++);
		if (argc > 0)
			ft_printf(" ");
	}
	if (opt == 0)
		ft_printf("\n");
	return (EXIT_SUCCESS);
}
