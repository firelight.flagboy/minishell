/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pwd.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/30 15:32:47 by fbenneto          #+#    #+#             */
/*   Updated: 2018/02/01 14:40:30 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

int		pwd(int argc, char **argv, t_env **env)
{
	char	buf[2048];

	(void)argc;
	(void)argv;
	(void)env;
	ft_printf("%s\n", getcwd(buf, 2048));
	return (0);
}
