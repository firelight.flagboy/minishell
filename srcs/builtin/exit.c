/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   exit.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/23 11:08:54 by fbenneto          #+#    #+#             */
/*   Updated: 2018/01/31 15:18:05 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

int			f_exit(int argc, char **argv, t_env **env)
{
	if (argc > 2)
		return (ft_warnx_errn(argv[0], (g_errn = ERR_ARGTOOMUCH)));
	ft_env_free_all(env);
	ft_free_tchar_node(argv);
	if (argc == 1)
		exit(*get_res());
	exit(ft_atoi_cte(argv[1]));
}
