/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   yes.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/31 15:45:24 by fbenneto          #+#    #+#             */
/*   Updated: 2018/02/01 14:41:41 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

static int	*get_ne(void)
{
	static int ne;

	return (&ne);
}

void		cte(int sig)
{
	(void)sig;
	signal(SIGINT, handler);
	*get_ne() = 0;
}

int			yes(int argc, char **argv, t_env **env)
{
	char	*s;
	int		*t;

	signal(SIGINT, cte);
	t = get_ne();
	*t = 1;
	s = "y";
	(void)env;
	if (argc > 1)
		s = argv[1];
	while (*t)
		ft_printf("%s\n", s);
	return (0);
}
