/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   setenv.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/24 13:51:09 by fbenneto          #+#    #+#             */
/*   Updated: 2018/02/01 14:40:59 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

int		ft_setenv_main(int argc, char **argv, t_env **env)
{
	t_env *node;

	if (argc < 3)
	{
		ft_dprintf(2, "usage :%s [key] [value]\n%s: missing argument\n",\
		argv[0], argv[0]);
		return (0);
	}
	else if (argc > 3)
	{
		ft_dprintf(2, "usage :%s [key] [value]\n%s: too much arguments\n",\
		argv[0], argv[0]);
		return (0);
	}
	if (ft_strchr(argv[1], '=') || ft_strchr(argv[2], '='))
		return (ft_warnx(argv[0], "argument must not have '=' in it"));
	ft_setenv(env, argv[1], argv[2]);
	node = *env;
	while (node)
	{
		ft_printf("%s=%s\n", node->key, node->value);
		node = node->next;
	}
	return (0);
}
