/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   clear.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/23 14:11:25 by fbenneto          #+#    #+#             */
/*   Updated: 2018/01/30 15:02:13 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

int		clear(int argc, char **argv, t_env **env)
{
	(void)argc;
	(void)argv;
	(void)env;
	ft_printf("\033[2J\033[1;1H");
	return (0);
}
