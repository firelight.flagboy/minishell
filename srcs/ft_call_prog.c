/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_call_prog.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/22 16:44:48 by fbenneto          #+#    #+#             */
/*   Updated: 2018/02/06 13:24:53 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

static int	ft_exe(char **av, char *pt)
{
	char	buf[2048];
	int		res;

	ft_strcpy(buf, pt);
	ft_strcat(buf, "/");
	ft_strcat(buf, av[0]);
	g_errn = ERR_OK;
	if ((res = ft_access(buf, X_OK)) == 0)
	{
		return (1);
	}
	return (res);
}

void		ft_exit_code(int *res, char *name)
{
	int	i;

	if (WIFEXITED(*res))
		*res = WEXITSTATUS(*res);
	else if (WIFSIGNALED(*res))
	{
		i = WTERMSIG(*res);
		if (i != SIGINT)
			ft_printf("minishell : %s: %s %d\n", name, ft_strsig(i), i);
	}
	else if (WIFSTOPPED(*res))
	{
		i = WSTOPSIG(*res);
		ft_printf("minishell : %s: %s\n", name, ft_strsig(i), i);
	}
}

int			ft_fork(char **av, char ***t, int *res, int i)
{
	char	buf[2048];

	ft_strcpy(buf, t[0][i]);
	ft_strcat(buf, "/");
	ft_strcat(buf, av[0]);
	g_pid = fork();
	if (g_pid == 0)
	{
		if (execve(buf, av, t[1]) == -1)
			ft_error("execve", "error");
	}
	else
	{
		if (waitpid(g_pid, res, 0) == -1)
			return (ft_warnx("waitpid", "error"));
		ft_exit_code(res, av[0]);
	}
	return (0);
}

int			ft_call_path(int argc, char **argv, t_env **environ, int *res)
{
	char	**tab[2];
	char	*path;
	int		i;

	(void)argc;
	path = ft_getenv(environ, "PATH");
	if (path == NULL || *path == 0)
		return (ft_warnx_errn("minishell", (g_errn = ERR_NOPATH)));
	if (!(tab[0] = ft_strsplit(path, ':')))
		ft_error2_errn("minishell", argv[0], (g_errn = ERR_MALLOC));
	i = -1;
	while (tab[0][++i])
		if (ft_exe(argv, tab[0][i]) && g_errn != ERR_NOENT)
			break ;
	if (g_errn == ERR_NOENT)
	{
		ft_free_tchar(tab[0]);
		return (ft_warn2_errn("minishell", argv[0], (g_errn = ERR_NOCMD)));
	}
	if (!(tab[1] = ft_env_list_tab(environ)))
		ft_error2_errn("minishell", argv[0], g_errn);
	ft_fork(argv, tab, res, i);
	ft_free_tchar_d(tab);
	return (*res);
}

int			ft_call_prog(int argc, char **argv, t_env **environ, int *res)
{
	t_bult	*bult;
	int		i;

	if ((ft_strcmp(argv[0], ".") == 0 || ft_strcmp(argv[0], "..") == 0)\
	&& (g_errn = ERR_NOCMD))
		return (ft_warn2_errn("minishell", argv[0], g_errn));
	if (ft_strchr(argv[0], '/'))
		return (ft_direct_cmd(argv, environ, res));
	i = -1;
	bult = ft_get_tbult();
	while (bult[++i].name)
		if (ft_strcmp(bult[i].name, argv[0]) == 0)
			return (bult[i].s(argc, argv, environ));
	*res = 0;
	return (ft_call_path(argc, argv, environ, res));
}
