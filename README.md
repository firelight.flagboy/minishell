# minishell
project where you have to code a simple shell interface

## Info

### Pdf to Projects
* **[link to pdf](https://cdn.intra.42.fr/pdf/pdf/107/minishell.fr.pdf)**

### Man
* **[man stat](http://manpagesfr.free.fr/man/man2/stat.2.html)**
* **[man lstat](http://manpagesfr.free.fr/man/man2/stat.2.html)**
* **[man readdir](http://manpagesfr.free.fr/man/man3/readdir.3.html)**
* **[man opendir](http://manpagesfr.free.fr/man/man3/opendir.3.html)**
* **[man closedir](http://manpagesfr.free.fr/man/man3/closedir.3.html)**
* **[man access](http://manpagesfr.free.fr/man/man2/access.2.html)**
* **[man fork](http://manpagesfr.free.fr/man/man2/fork.2.html)**
* **[man execve](http://manpagesfr.free.fr/man/man2/execve.2.html)**
* **[man wait](http://manpagesfr.free.fr/man/man2/wait.2.html)**
* **[man wait 3 /4](http://manpagesfr.free.fr/man/man2/wait4.2.html)**
* **[man signal](http://manpagesfr.free.fr/man/man2/signal.2.html)**
* **[man kill](http://manpagesfr.free.fr/man/man2/kill.2.html)**
* **[man exit](http://manpagesfr.free.fr/man/man3/exit.3.html)**
* **[man chdir](http://manpagesfr.free.fr/man/man2/chdir.2.html)**
* **[man getcwd](http://manpagesfr.free.fr/man/man3/getcwd.3.html)**

***
### Use
```
make minishell
./minishell
```
#### In minishell
```
$>yourcommand with argument
result of your command with 'with' 'argument'
$>yourcommand "with "" in argument"
result of yourcommand with 'with "" in argument'
$>yourcommand with\ in\ it
result of yourcommand with 'with in it'
```
***
### Builtin

* **cd** :
    * change current directory
* **clear** :
    * clear the terminal
* **echo** :
    * dysplay argument send to i'm with one space between each one
    * option `-n` : dysplay argument with no newline
* **env** :
    * with no argument dysplay the content of environ
    * with argument, try to exe the first argument send  the next one to him
        * ex : `env echo bonjour` call `echo` with 'bonjour' in argument 
    * option `-i`, dysplay/ send new environ
* **exit** :
    * no argument :
        * exit with the last res value of command
            * ex : if `ls` return 0 then exit will return 0
    * with argument :
        * exit with the value of the first argument (see `atoi`)
            * `exit 42` will exit with the return value of `42`
* **pwd** :
    * dysplay the current directory
* **setenv** :
    * usage `setenv [key] [value]`
        * will create the key `[key]` if not already define and give him the value `[value]`
        * else will redifine the `[key]` with the value `[value]`
* **unsetenv** :
    * usage : `unsetenv [key ...]`
        * unsetenv the `[key]` form the environ, do nothing if not exist
* **yes** :
    * with no argument will dysplay an infinity of 'y' with newline
    * with argument will dysplay an infinity of the first one with newline
***
