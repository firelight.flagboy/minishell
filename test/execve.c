/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   execve.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/18 16:38:11 by fbenneto          #+#    #+#             */
/*   Updated: 2018/01/19 09:02:05 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <stdio.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <string.h>
#include <stdlib.h>

int		main(int argc, char **argv)
{
	char	*newargv[] = {"echo.sh", "-l", NULL};
	char	*newenviron[] = {NULL};
	int		st;
	pid_t	pid;

	pid = 0;
	printf("parent\n");
	pid = fork();
	if (pid == 0)
	{
		printf("processus fils\n");
		if (execve(newargv[0], newargv, newenviron) == -1)
		{
			printf("minishell : %s: %s\n", newargv[0], strerror(errno));
			exit(EXIT_FAILURE);
		}
		printf("done fils\n");
	}
	waitpid(-1, &st, 0);
	printf("le programme a retourner %d done pid :%d\n", st, pid);
	return (EXIT_SUCCESS);
}
