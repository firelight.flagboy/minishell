/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   echo.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/19 14:53:23 by fbenneto          #+#    #+#             */
/*   Updated: 2018/01/19 15:22:49 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

static int	ft_init_opt(char *opt, int *res)
{
	char	*charset;
	int		i;

	charset = "n";
	while (*opt)
	{
		i = 0;
		while (charset[i])
		{
			if (*opt == charset[i])
				*res = *res | (1 << i);
			i++;
		}
		if (charset[i] == 0)
		{
			*res = 0;
			return (-1);
		}
		opt++;
	}
	return (0);
}

static void	ft_get_opt_echo(int *argc, char ***argv, int *opt)
{
	int	i;
	int		res;
	char	**ts;

	i = 0;
	res = 0;
	ts = *argv;
	while (ts[i])
	{
		if (ts[i][0] != '-')
			break ;
		if (ft_init_opt((ts[i] + 1), &res) == -1)
			break ;
		i++;
	}
	*argv = *argv + i;
	*argc = *argc - i;
	*opt = res;
}

int		main(int argc, char **argv)
{
	int		opt;

	if (argc == 0)
		ft_printf("\n");
	opt = 0;
	argc--;
	argv++;
	ft_get_opt_echo(&argc, &argv, &opt);
	while (argc)
	{
		argc--;
		ft_printf("%s", *argv++);
		if (argc > 0)
			ft_printf(" ");
	}
	if (opt == 0)
		ft_printf("\n");
	return (0);
}
