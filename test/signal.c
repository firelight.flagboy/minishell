/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   signal.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/19 09:13:52 by fbenneto          #+#    #+#             */
/*   Updated: 2018/01/26 12:42:26 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

# include <sys/types.h>
# include <sys/stat.h>
# include <sys/wait.h>
# include <sys/time.h>
# include <sys/resource.h>
# include <unistd.h>
# include <signal.h>
# include <dirent.h>
# include <stdlib.h>
# include <stdio.h>
# include <fcntl.h>
# include <string.h>
# include <stdio.h>
# include <errno.h>

// static void catch_function(int signo) {
// 	puts("Interactive attention signal caught.");
// }

// int main(void) {
// 	if (signal(SIGINT, catch_function) == SIG_ERR) {
// 		fputs("An error occurred while setting a signal handler.\n", stderr);
// 		return EXIT_FAILURE;
// 	}
// 	puts("Raising the interactive attention signal.");
// 	sleep(5);
// 	// if (raise(SIGINT) != 0) {
// 		// fputs("Error raising the signal.\n", stderr);
// 		// return EXIT_FAILURE;
// 	// }
// 	puts("Exiting.");
// 	return EXIT_SUCCESS;
// 	// exiting after raising signal
// }
pid_t pid;

void		handler(int sig)
{
	if (pid != 0)
	{
		printf("pid :%d want kill :%d", getpid(), pid);
		kill(pid, SIGKILL);
	}
	else
		printf("%d\n", sig);
}

int		main(int argc, char **argv)
{
	int		st;

	signal(SIGINT, handler);
	(void)argc;
	pid = fork();
	if (pid == 0)
	{
		while (1)
		{
			printf("child\n");
			sleep(1);
		}
	}
	else if (pid < 0)
		printf("%s: fork: %s\n", argv[0], strerror(errno));
	else
	{
		printf("parent\n");
		waitpid(pid, &st, 0);
	}
}
