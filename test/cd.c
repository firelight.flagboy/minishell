/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cd.c                                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/19 15:41:36 by fbenneto          #+#    #+#             */
/*   Updated: 2018/01/22 10:35:50 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"
#include <errno.h>

int		main(int argc, char **argv, char **environ)
{
	char	*pwd;
	char	*last_pwd;
	char	*home;
	char	*zone;
	char	buf[1024];
	int 	res;

	pwd = ft_getenv(environ, "PWD");
	home = ft_getenv(environ, "HOME");
	last_pwd = ft_getenv(environ, "OLDPWD");
	printf("pwd: %s\nold pwd: %s\nhome: %s\n", pwd, last_pwd, home);
	printf("%s %s %s\n", getenv("PWD"), getenv("OLDPWD"), getenv("HOME"));
	if (argc > 2)
		return (ft_error("cd", "too many argument"));
	else if (argc == 1)
		zone = home;
	else if (ft_strcmp(argv[1], "~") == 0)
		zone = home;
	else if (ft_strcmp(argv[1], "-") == 0)
		zone = last_pwd;
	else
		zone = argv[1];
	printf("zone :%s\n", zone);
	if ((getcwd(buf, 1024)))
		printf("buf :%s\n", buf);
	res = chdir(zone);
	if (res == -1)
		printf("%s: %s: %s\n", argv[0], zone, strerror(errno));
	if ((getcwd(buf, 1024)))
		printf("buf :%s\n", buf);
	return (res);
}
