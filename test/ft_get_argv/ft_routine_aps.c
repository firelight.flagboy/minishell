/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_routine_aps.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/30 09:56:56 by fbenneto          #+#    #+#             */
/*   Updated: 2018/01/30 11:23:28 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_get_argv.h"

char	*routine_aps(t_list *t, char *s, size_t i)
{
	char	*d;

	d = s;
	while (*d && *d != 0x22)
		d++;
	if (*d == 0 && d != s)
		return (NULL);
	if (*s == '~')
	{
		ft_add_env(t->s, "$HOME", &i);
		s++;
	}
	while (*s && *s != 0x22)
	{
		if (*s == '$')
			s = ft_add_env(t->s, s, &i);
		else
			t->s[i++] = *s;
		s++;
	}
	t->s[i] = '\0';
	if (*s == 0x22)
		return (routine_aps(t, s + 1, i));
	return (s);
}
