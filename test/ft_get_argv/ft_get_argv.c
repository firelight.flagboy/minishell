/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_get_argv.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/29 09:18:19 by fbenneto          #+#    #+#             */
/*   Updated: 2018/01/30 12:46:11 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_get_argv.h"

int		ft_isspace(int c)
{
	return (c == ' ' || c == '\t' || c == '\v' || c == '\r' || c == '\n');
}

char *ft_add_env(char *t, char *s, size_t *i)
{
	char	*env;
	char	env_n[1024];
	int		y;

	s++;
	y = 0;
	while (*s && ((*s >= '0' && *s <= '9') || (*s >= 'a' && *s <= 'z') || (*s >= 'A' && *s <= 'Z')))
		env_n[y++] = *s++;
	env_n[y] = 0;
	t[*i] = 0;
	if (!(env = getenv(env_n))) //remove to ft_get_env
		return (s);
	y = 0;
	while (*env)
		t[*i + y++] = *env++;
	t[*i + y] = 0;
	*i = *i + y;
	return (s - 1);
}

static char	*routine(t_list *t, char *s)
{
	while (*s && ft_isspace(*s))
		s++;
	if (*s == 0x22)
		return (routine_aps(t, s + 1, 0));
	else
		return (routine_nor(t, s, 0));
}

static char	*ft_strncpy(char *d, char *s, size_t len)
{
	size_t	i;

	i = 0;
	while (*s && len)
	{
		d[i] = *s++;
		i++;
		len--;
	}
	d[i] = 0;
	return (d);
}

int		ft_return(char ***t, t_list **headref, size_t i)
{
	char	**b;
	t_list	*node;
	t_list	*tmp;
	size_t	len;

	if (!(b = (char**)malloc((i + 1) * sizeof(char *))))
		return(-1);
	i = 0;
	node = *headref;
	while (node)
	{
		len = strlen(node->s);
		if (!(b[i] = (char*)malloc((len + 1) * sizeof(char))))
			return (-1);
		ft_strncpy(b[i], node->s, len);
		tmp = node;
		node = node->next;
		free(tmp);
		i++;
	}
	b[i] = 0;
	*t = b;
	return (0);
}

static	int	ft_sort(t_list **head)
{
	t_list *node;
	t_list *tmp;

	node = *head;
	while (node)
	{
		tmp = node;
		node = node->next;
		free(tmp);
	}
	return (-1);
}

int		ft_get_argv(char *s, char ***t)
{
	long i;
	t_list	*head;
	t_list	*node;

	if (!(head = ft_new_tlist()))
		return (-1);
	node = head;
	if ((s = routine(node, s)) == NULL)
			return (-1);
	i = 0;
	while (*s != '\0')
	{
		if (i > ARG_MAX)
			return (ft_sort(&head));
		if (!(node->next = ft_new_tlist()))
			return (ft_sort(&head));
		node = node->next;
		if ((s = routine(node, s)) == NULL)
			return (ft_sort(&head));
		i++;
	}
	i = ft_len_list(head);
	if (ft_return(t, &head, i) == -1)
		return (-1);
	return (i);
}
