/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_routine_nor.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/30 10:00:19 by fbenneto          #+#    #+#             */
/*   Updated: 2018/01/30 12:47:58 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_get_argv.h"

char *routine_nor(t_list *t, char *s, size_t i)
{
	if (*s == '~')
	{
		ft_add_env(t->s, "$HOME", &i);
		s++;
	}
	while (*s && ft_isspace(*s) == 0)
	{
		if (*s == '$')
			s = ft_add_env(t->s, s, &i);
		else if (*s == 0x5c && *(s + 1) != '\0')
			t->s[i++] = *(1 + s++);
		else if (*s != 0x5c)
			t->s[i++] = *s;
		s++;
	}
	t->s[i] = '\0';
	return (s);
}
