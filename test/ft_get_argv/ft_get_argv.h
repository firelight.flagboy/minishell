/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_get_argv.h                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/29 10:36:40 by fbenneto          #+#    #+#             */
/*   Updated: 2018/01/30 12:49:59 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_GET_ARGV_H
# define FT_GET_ARGV_H

# include <stdlib.h>
# include <stdio.h>
# include <limits.h>
# include <string.h>
# include <unistd.h>

# ifndef ARG_MAX
#  define ARG_MAX (256 * 1024)
# endif

# ifndef ARG_LEN_MAX
#  define ARG_LEN_MAX 2048
# endif

typedef struct	s_list
{
	char			s[2048];
	struct s_list	*next;
}				t_list;

t_list	*ft_new_tlist(void);
char		*routine_aps(t_list *t, char *s, size_t i);
char		*routine_nor(t_list *t, char *s, size_t i);
char		*ft_add_env(char *t, char *s, size_t *i);
size_t		ft_len_env(char **s);
int			ft_isspace(int c);
size_t		ft_len_list(t_list *head);
int			ft_get_argv(char *s, char ***t);
#endif
