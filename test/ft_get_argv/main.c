/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/29 09:18:22 by fbenneto          #+#    #+#             */
/*   Updated: 2018/01/30 12:43:37 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

# include "ft_get_argv.h"

int		main(void)
{
	char	*s[] = {
	"",
	"echo une commande classique",
	"		un peux			plus d		ur",
	"cette \"fois si avec de aps\"",
	"macher \"t'il toujours\\ là ?\"",
	"peut\\ t'on\\ le\\ faire ?",
	"utilisation de l'env $home",
	"accru $HOME",
	"double $Home",
	" 1 $HOME/Documents",
	" 2 \"$HOME/Documents\"",
	"un truct \"mal parse",
	"tj un truc mal\"",
	"~/repo/ft_ls/ft_ls ~/ ~/",
	NULL
	};
	char	**t;
	// char	*b = 0;
	int		d = 0;
	int		i;
	int		res;

	printf("%s\n%s\n", getenv("HOME"), getenv("$HOME"));
	while (s[d])
	{
		i = 0;
		// b = strdup(s[d]);
		printf("str to split:{%s}\n", s[d]);
		res = ft_get_argv(s[d], &t);
		printf("%d\n", res);
		if (res != -1)
		{
				while (t[i]){
					printf("%s\n", t[i]);
					free(t[i++]);
				}
			free(t);
		}
		d++;
	}
	sleep(5);
	return (0);
}

