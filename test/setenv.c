/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   setenv.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/19 10:59:32 by fbenneto          #+#    #+#             */
/*   Updated: 2018/01/19 11:01:11 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int		main(int argc, char **argv, char **environ)
{
	argc = 0;
	while (*argv)
		argc = ft_printf("%s\n", *argv++);
	if (argc)
		ft_printf("\n");
	while (*environ)
		ft_printf("%s\n", *environ++);
}
