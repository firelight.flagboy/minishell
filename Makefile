# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2017/12/27 12:36:31 by fbenneto          #+#    #+#              #
#    Updated: 2018/02/09 12:47:01 by fbenneto         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

##########
# TARGET #
##########

NAME=minishell

#######
# LIB #
#######

LIB_PRINTF_DIR =\
	ft_printf/
LIB_PRINTF_NAME =\
	libftprintf.a

LIB_PRINTF = $(addprefix $(LIB_PRINTF_DIR), $(LIB_PRINTF_NAME))
############
# COMPILER #
############

CC =clang
UNAME_S := $(shell uname -s)
ifeq ($(UNAME_S), Linux)
	CFLAGS += -O3
endif
ifeq ($(UNAME_S), Darwin)
	CFLAGS += -Wall -Werror -Wextra -O3
endif

##########
# HEADER #
##########

INC_DIR_PRINTF	=\
	./ft_printf/includes/
INC_NAME_PRINTF	=\
	ft_printf_typedef.h\
	ft_printf_header_fc.h\
	ft_printf_d.h\
	ft_printf.h\

INCLUDE_PRINTF	= $(addprefix $(INC_DIR_PRINTF), $(INC_NAME_PRINTF))

INC_PRINTF	= -I $(INC_DIR_PRINTF)

INC_DIR =\
	./includes/

INC_NAME = \
	get_next_line.h\
	libft.h\
	my_errno.h\
	minishell_struct.h\
	minishell_define.h\
	minishell_typedef.h\
	minishell.h\

INCLUDE	= $(addprefix $(INC_DIR), $(INC_NAME))
INC	= -I $(INC_DIR)

#######
# SRC #
#######

SRC_DIR	= ./srcs/

SRC_BUILT_DIR= builtin/

SRC_BUILT_NAME=\
	cd.c\
	env.c\
	pwd.c\
	yes.c\
	echo.c\
	exit.c\
	clear.c\
	setenv.c\
	unsetenv.c\

SRC_BUILT = $(addprefix $(SRC_BUILT_DIR), $(SRC_BUILT_NAME))

SRC_ERRNO_DIR = errno/

SRC_ERRNO_NAME =\
	my_errno.c\
	my_errno_arg.c\
	my_errno_str.c\
	my_errno_atoi.c\
	my_errno_error.c\
	my_errno_warnx.c\

SRC_ERRNO = $(addprefix $(SRC_ERRNO_DIR), $(SRC_ERRNO_NAME))

SRC_LIST_DIR	= ft_list/

SRC_LIST_NAME	=\
	ft_list_env.c\
	ft_list_env_2.c\
	ft_list_argv.c\

SRC_LIST		= $(addprefix $(SRC_LIST_DIR), $(SRC_LIST_NAME))

SRC_LIBC_DIR	= libc/

SRC_LIBC_NAME	=\
	ft_str.c\
	ft_str_2.c\
	ft_str_3.c\
	ft_split.c\
	ft_tchar.c\
	ft_atoi_cte.c\
	get_next_line.c\

SRC_LIBC		= $(addprefix $(SRC_LIBC_DIR), $(SRC_LIBC_NAME))

SRC_NAME =\
	$(SRC_ERRNO)\
	$(SRC_BUILT)\
	$(SRC_LIBC)\
	$(SRC_LIST)\
	ft_promt.c\
	ft_type.c\
	minishell.c\
	ft_access.c\
	ft_call_prog_2.c\
	ft_get_argv.c\
	ft_get_argv_2.c\
	ft_env.c\
	ft_singleton.c\
	ft_init_tbult.c\
	ft_call_prog.c\

SRC_ALL		=  $(addprefix $(SRC_DIR), $(SRC_NAME))
#######
# OBJ #
#######

OBJ_DIR			= ./obj/

OBJ_NAME	= $(SRC_NAME:.c=.o)

OBJ_ALL			= $(addprefix $(OBJ_DIR), $(OBJ_NAME))

#########
# MACRO #
#########

NC		= "\\033[0m"
RED		= "\\033[31m"
GREEN	= "\\033[32m"
YELLOW	= "\\033[33m"
BLUE	= "\\033[34m"
MAJENTA	= "\\033[35m"
CYAN	= "\\033[36m"
BOLD	= "\\033[1m"
CHEK	= "✓"
OK		= "$(GREEN)$(CHEK)$(NC)"

#########
# RULES #
#########

all : $(NAME)

$(NAME) : $(LIB_PRINTF) $(OBJ_DIR) $(OBJ_ALL)
	@printf "[minishell: $@]\n"
	@printf "\tcompile binary $(BOLD)$(CYAN)$@\t"
	@$(CC) $(CFLAGS) -o $@ $(OBJ_ALL) $(LIB_PRINTF)
	@printf $(NC)$(OK)'\n'

$(OBJ_DIR) :
	@printf "[minishell: $@]\n"
	@printf "\tcreating folder $(MAJENTA)$(BOLD)$(OBJ_DIR)$(NC)\t"
	@mkdir -p $(OBJ_DIR)
	@printf $(OK)'\n'
	@printf "\tcreating folder $(MAJENTA)$(BOLD)$(SRC_BUILT_DIR)$(NC)\t"
	@mkdir -p $(OBJ_DIR)/$(SRC_BUILT_DIR)
	@printf $(OK)'\n'
	@printf "\tcreating folder $(MAJENTA)$(BOLD)$(SRC_ERRNO_DIR)$(NC)\t"
	@mkdir -p $(OBJ_DIR)/$(SRC_ERRNO_DIR)
	@printf $(OK)'\n'
	@printf "\tcreating folder $(MAJENTA)$(BOLD)$(SRC_LIST_DIR)$(NC)\t"
	@mkdir -p $(OBJ_DIR)/$(SRC_LIST_DIR)
	@printf $(OK)'\n'
	@printf "\tcreating folder $(MAJENTA)$(BOLD)$(SRC_LIBC_DIR)$(NC)\t"
	@mkdir -p $(OBJ_DIR)/$(SRC_LIBC_DIR)
	@printf $(OK)'\n'

$(OBJ_DIR)%.o: $(SRC_DIR)%.c $(INCLUDE) $(INCLUDE_PRINTF)
	@printf "\tcompile $(BOLD)$(YELLOW)$<$(NC)\t"
	@$(CC) $(CFLAGS) -o $@ -c $< $(INC) $(INC_PRINTF)
	@printf $(OK)'\n'

$(SRC_ALL): $(INCLUDE)

$(LIB_PRINTF) :
	@make -C $(LIB_PRINTF_DIR) $(LIB_PRINTF_NAME)

clean : $(OBJ_DIR)
	@make -C $(LIB_PRINTF_DIR) clean
	@printf "[minishell: $@]\n\trm all $(BOLD)$(RED)obj file"
	@rm -rf $(OBJ_DIR)
	@printf '\t'$(NC)$(OK)'\n'

norme : $(SRC_ALL) $(INCLUDE)
	@make -C $(LIB_PRINTF_DIR) norme
	@printf "[minishell: $@]\n"
	@norminette $^ | grep -B 1 "Warning\|Error" || true
	@printf "$(GREEN)$(BOLD)DONE$(NC)\n"

fclean : clean
	@make -C $(LIB_PRINTF_DIR) naelc
	@printf "[minishell: $@]\n\trm $(BOLD)$(CYAN)$(NAME)"
	@rm -f $(NAME)
	@printf '\t'$(NC)$(OK)'\n'

proper :
	@make -C ./ all
	@make -C ./ clean

re :
	@make -C ./ fclean
	@make -C ./ all

run :
	@make -C ./ all
	./minishell || true

.PHONY: proper re norme all fclean clean naelc run
