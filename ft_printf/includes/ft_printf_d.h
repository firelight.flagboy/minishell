/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf_d.h                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/05 15:34:31 by fbenneto          #+#    #+#             */
/*   Updated: 2018/01/16 12:52:19 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_PRINTF_D_H
# define FT_PRINTF_D_H
/*
** Header that contains all perso define
*/
# ifndef NB_FC
#  define NB_FC 16
# endif

# ifndef NB_COLOR
#  define NB_COLOR 22
# endif

# ifndef BUFF_SIZE_PRINTF
#  define BUFF_SIZE_PRINTF 4096
# endif

# ifndef OPEN_MAX
#  define OPEN_MAX 10240
# endif

#endif
